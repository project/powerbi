INTRODUCTION
--------
This module supports Microsoft Power BI Embedded service, including dashboards,
reports, report visuals, Q&A, and tiles in Drupal.

CONFIGURATION
------------

1. Install module from https://drupal.org/project/powerbi or you directly put
   your extracted powerbi module folder to "sites/all/modules/" directory and
   enable it by visiting the URL: "http://example.com/admin/modules".

2. Visit powerbi setting page at
  "http://example.com/admin/config/services/powerbi/settings" and enter the
  required information under the tab "Account Information" and
  "Application Information".

3. Create menu link with the following path to embed powerbi data in you site:
  http://example.com/pbi/dashboards/data - For dashboard
  http://example.com/pbi/reports/data - For Reports

REQUIREMENTS
------------


Reference
=========
https://github.com/Microsoft/PowerBI-JavaScript/wiki/
